Apertamente
=======
![](website/static/MySmartOpinionIcon.png)<br/>
### Premessa
Il progetto ha avuto come obiettivo quello di realizzare una piattaforma web e un’App Android per la somministrazione di questionari di gradimento e la profilazione dei clienti di esercizi commerciali. Ad esempio un cliente che va in un ristorante convenzionato con l’app MySmartOpinion può trovare un QR code sullo scontrino, oppure su un poster a parete nel locale e con l’App Android può scannerizzare tale QR code per rispondere a un questionario di gradimento sul cibo e/o sul servizio ricevuto in cambio di un buono sconto o di punti che possono essere convertiti in buoni spesa.
La richiesta di sviluppare un’applicazione di questo tipo è stata fatta al nostro Istituto dall’azienda ApertaMente s.r.l. di Monza che opera da anni nel settore delle ricerche di mercato. Lo sviluppo dell’applicazione è stata affidata alle classi 5IA e 5IB dell’indirizzo Informatica-Telecomunicazioni – articolazione Informatica con il coordinamento dei docenti di Informatica Proff. Malafronte e Petracca. Tale progetto è stato inquadrato come attività di Alternanza Scuola Lavoro (ASL) per gli studenti delle suddette classi, da espletarsi sia durante parte delle ore curricolari di alcune delle materie di indirizzo sia durante l’attività di studio a casa.
### Il risultato prodotto
La piattaforma che è stata sviluppata si chiama MySmartOpinion e si compone di una applicazione Web accessibile al personale di ApertaMente e di un’App Android utilizzabile da tutti.

La piattaforma web MySmartOpinion permette di:
*	Creare questionari
*	Creare filtri per l’individuazione di un potenziale target di inoltro di questionari e di notifiche (ad esempio fare in modo che la notifica di una promozione venga inoltrata solo a clienti di una determinata città, maschi, di età compresa tra i 30 e 40 anni, etc..)
*	Inviare notifiche agli utenti dell’app Android su promozioni in corso e questionari di gradimento
* Visualizzare report e grafici statistici sulle risposte ottenute
*	Esportare i dati dei questionari in file utilizzabili in applicazioni di terze parti per l’analisi statistica approfondita
* Esportare i questionari somministrati in formato pdf
*	Gestire gli esercenti per i quali viene realizzata la campagna di marketing

L’applicazione Android MySmartOpinion consente di:
*	Scannerizzare un QR che lancia un questionario sull’applicazione
*	Ricevere notifiche push su promozioni in corso e questionari
*	Gestire il saldo dei punti dell’utente
*	Ottenere buoni acquisto al raggiungimento di soglie punti prefissate


Applicazione Android
--------------------

[![link per l'installazione di MySmartOpinion](website/static/MySmartOpinionScreen2.png)]( https://play.google.com/apps/testing/com.apertamente.mysmoandroid "MySmartOpinion Test")
Download
--------

## Download dell'[apk di testing per Android][2]

## Video
[![Video dello spot di Apertamente](website/static/MySmartOpinionVideoImage.png)](https://youtu.be/t9hSzeDbPis "MySmartOpinion Spot")
<br/>MySmartOpinion - the spot



## Screenshots
![](website/static/MySmartOpinionScreen1.png)
![](website/static/MySmartOpinionScreen3.png)
![](website/static/MySmartOpinionScreen4.png)


-------------------------------
La scheda descrittiva del progetto è scaricabile a questo [link][3]

Per ulteriori informazioni sul progetto contattare l'[Istituto Alessandro Greppi][1] di Monticello Brianza (Lc)




Licenza
--------
    Copyright 2018 IISS "Alessandro Greppi" - Monticello Brianza (Lc),
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 [1]: http://www.issgreppi.gov.it
 [2]: https://play.google.com/apps/testing/com.apertamente.mysmoandroid
 [3]: https://gitlab.com/GreppiShare/ApertaMenteBuild/raw/master/website/static/ApertaMente.pdf
